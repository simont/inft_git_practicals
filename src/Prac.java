public class Prac {

	public Prac() {

		System.out.println('Starting program');
		
		System.out.println('Testing number: 3, expecting output: 1 1 2 3');
		
		start(3);
	}
	
	public String start(int number) {
		
		String sequence = "";
		while ( number >= 0 ) {
			sequence += Integer.toString(fibbbonaci(number));
			
			number--;
		}
		
		return sequence;
	}
	
	public int fibbonaci(int thenumber) {
		
	    if(thenumber == 0)
	        return 0;
	    else if(thenumber == 1)
	      return 1;
	   else
	      return fibonacci(thenumber - 1) + fibonacci(thenumber - 2);
	}
	
}
