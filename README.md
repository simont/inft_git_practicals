# Practical for Week 6

Repository with base commits, branches, and instructions for practising git commands. 

## Commands
This prac uses: 

1. git clone
2. git rebase
3. git blame
4. git bisect

In addition: 

1. BASH scripts
2. git aliases
3. git tags
