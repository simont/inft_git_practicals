This practical covers a range of git commands. 

# Stages
Each stage uses some git command as it's focus. Remember to use: 
	git log
	git status
to see what's going on in your repository. 

## Stage 1: Obtaining the repository

This stage uses `git clone` to make a local copy of a remote 
repository. 

1. We want our local repository to be in a folder called "W06-Practical". 
2. Clone the repository from GitLab. 


